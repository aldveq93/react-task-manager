import React, {useState} from 'react';
import {Form, Alert , Button} from 'react-bootstrap'; 
import { v4 as uuidv4 } from 'uuid';
import './TaskForm.scss';

const TaskForm = ({getActivityFromForm}) => {
    // State of the form, creating the object from the form
    const [activityData, handleActivityData] = useState({
        activity: '',
        hour: '',
        description: ''
    });

    // Function to populate the state of the form
    const getActivityData = e => {
        handleActivityData({
            ...activityData,
            [e.target.name]: e.target.value
        })
    }

    // State to show feedback to the user
    const [feedback, handleFeedback] = useState(false);

    // Getting the data from the user to handle the submit form event
    const {activity, hour, description} = activityData;

    // Function to submit the form to the general state
    const submitActivityForm = e => {
        e.preventDefault()

        if(activity === '' || hour === '' || description === '') {
            handleFeedback(true);
            setTimeout(() => {handleFeedback(false);},4000)
        } else {
            activityData.id = uuidv4();       
            // Sending the activity object to the general state to render the activities cards
            getActivityFromForm(activityData);
            // Reseting the form
            handleActivityData({
                activity: '',
                hour: '',
                description: ''
            })
        }
    }

    return (
        <Form 
            className="form-task-manager"
            onSubmit={submitActivityForm}    
        >

            {feedback ? 
                <Alert className="feedback-animation mb-5" variant="danger" onClose={() => handleFeedback(false)} dismissible>
                    <Alert.Heading>Oh snap! You got an error!</Alert.Heading>
                    <p>
                        Please, fill in all the inputs!
                    </p>
                </Alert>
                : null
            }

            <Form.Group controlId="formBasicEmail">
                <Form.Label className="white-color">Activity</Form.Label>
                <Form.Control type="text" placeholder="Enter your activity" name="activity" value={activity} onChange={getActivityData} />
            </Form.Group>

            <Form.Group controlId="formBasicPassword">
                <Form.Label className="white-color">Hour</Form.Label>
                <Form.Control type="time" min="09:00" max="18:00" name="hour" value={hour} onChange={getActivityData} />
            </Form.Group>

            <Form.Group controlId="exampleForm.ControlTextarea1">
                <Form.Label className="white-color">Activity Description</Form.Label>
                <Form.Control as="textarea" rows="3" name="description" value={description} onChange={getActivityData} placeholder="Enter activity description" />
            </Form.Group>

            <Button variant="primary" type="submit">
                Add Activity
            </Button>
        </Form>
    );
};

export default TaskForm;
