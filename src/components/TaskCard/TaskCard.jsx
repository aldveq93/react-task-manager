import React from 'react';
import {Col, Card, Button} from 'react-bootstrap';
import './TaskCard.scss';

function TaskCard({activityObj, deleteActivity}) {

    const {activity, hour, description, id} = activityObj;

    return (
        <Col sm={12} md={6} className="mb-3">
            <Card className="task-card">
                <Card.Body>
                    <Card.Title className="task-card__title">{activity}</Card.Title>
                    <Card.Text className="task-card__desc">
                        {description}
                    </Card.Text>
                </Card.Body>
                <Card.Footer className ="d-flex justify-content-between align-items-center">
                    <small className="pr-2 task-card__hour">{hour}</small>
                    <Button variant="danger" onClick={() => {deleteActivity(id)}}><i className="icon-trash-empty"></i></Button>
                </Card.Footer>
            </Card>
        </Col>
    );
}

export default TaskCard;