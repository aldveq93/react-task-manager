import React, {useState, useEffect} from 'react';
import {Container, Row, Col, CardGroup} from 'react-bootstrap';
import TaskForm from './components/TaskForm/TaskForm';
import TaskCard from './components/TaskCard/TaskCard';

function App() {

  let activitiesFromLocalStorage = JSON.parse(localStorage.getItem('activities'));
  if(activitiesFromLocalStorage === null) {
    activitiesFromLocalStorage = [];
  }

  const [activities, handleActivities] = useState(activitiesFromLocalStorage);

  useEffect(() => {
    if(activitiesFromLocalStorage !== null) {
      localStorage.setItem('activities', JSON.stringify(activities));
    } else {
      localStorage.setItem('activities', JSON.stringify([]));
    }
  }, [activities, activitiesFromLocalStorage])

  const getActivityFromForm = activity => {
    handleActivities([
      ...activities,
      activity
    ])
  }

  const deleteActivity = id => {
    const arrayActivities = activities.filter(activity => activity.id !== id);
    handleActivities(arrayActivities);
  }

  return (
    <Container fluid>
      <Row>
        <Col xs={12} md={6} className="mb-5 mb-md-0">
          <Row>
              <Col xs={12} className="d-flex justify-content-center align-items-center mb-5">
                  <h1 className="white-color text-center">What are you going to do today?</h1>
              </Col>
              <Col xs={12} className="d-flex justify-content-center align-items-center">
                <TaskForm getActivityFromForm={getActivityFromForm}/>
              </Col>
          </Row>
        </Col>
        <Col xs={12} md={6}>
          <Row>
            <Col xs={12} className="d-flex justify-content-center align-items-center mb-5">
                {activities.length > 0 ? <h1 className="white-color text-center">TODO</h1> : <h1 className="white-color text-center">Create a new activity!</h1>}
            </Col>
            <Col xs={12} className="px-5 pb-5">
              <CardGroup className="row">

                {activities.map(activity => (
                  <TaskCard 
                    key={activity.id}
                    activityObj={activity}
                    deleteActivity={deleteActivity}
                  />
                ))}
              </CardGroup>
            </Col>
          </Row>  
        </Col>
      </Row>
    </Container>
  );
}

export default App;
